#!/usr/bin/env python

import os
import sys
import json
import subprocess
import requests
from datetime import datetime
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from hcpxnat.interface import HcpInterface

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

xnat_projects = ['CCF_HCA_ITK', 'CCF_HCD_ITK']
aspera_server = 'asp-connect1.wustl.edu'

cfg_file = os.path.join(os.path.expanduser('~'), ".hcpxnat_intradb.cfg")
intradb = HcpInterface(config=cfg_file)


def main():
    aspera_uuids = getAsperaUuids()
    mars_uuids = getOwncloudUuids('mars_raw_files.txt')
    relay_uuids = getOwncloudUuids('relay-raw-data-staging_files.txt')
    xnat_uuids = getXnatUuids()

    # print "\n-- Aspera UUIDS -- {} records --".format(len(aspera_uuids))
    # print aspera_uuids
    # print "\n-- Mars UUIDS --"
    # print mars_uuids
    # print "\n-- Relay UUIDS --"
    # print relay_uuids
    # sys.exit()

    intradbBackupReport(aspera_uuids, xnat_uuids)


def getAsperaUuids():
    """
    Parses and returns the UUIDs from the MARs file listing
    """
    command = "find /data/lifespanRAW/*"
    result = subprocess.check_output(command, shell=True)
    filenames = result.split('\n')[:-1]  # Exclude empty string from list

    return getUuidsFromFilenames(filenames)


def getOwncloudUuids(fname):
    all_uuids = []

    for host in os.listdir('/owncloud'):
        if "-relay" not in host:
            continue

        command = "cat /owncloud/{0}/stats/{1}".format(host, fname)
        result = subprocess.check_output(command, shell=True)
        filenames = result.split('\n')[:-1]
        host_uuids = getUuidsFromFilenames(filenames, host)
        all_uuids.extend(host_uuids)

    return all_uuids


def getUuidsFromFilenames(filenames, site=""):
    uuids = []

    for f in filenames:
        uuid_map = dict()
        uuid_map['filename'] = f
        try:
            uuid_str = f.split('_')[3]
        except Exception as e:
            # with open(log_file, 'a') as f:
            #     f.write("ERROR {}".format(f))
            #     f.write("Not a cplxraw file or improper file name format")
            #     f.write(e.message)
            print "{} doesn't look like a cplxraw file".format(f)
            continue

        if len(uuid_str) == 36:
            uuid_map['uuid'] = uuid_str
            uuid_map['site'] = site
            uuids.append(uuid_map)
        else:
            e = "ERROR adding " + uuid_str + ". Likely not a UUID"
            print e

    return uuids


def getXnatUuids():
    """
    Get the set of UUIDs for each scan in each session in each project
    """
    uuids = []

    for p in xnat_projects:
        # print "\n=== Project {} ===\n".format(p)
        sessions = intradb.getSessions(project=p)
        # print sessions

        for session in sessions:
            # print session
            # sys.exit()

            # Get session aquisition site
            uri = '/data/experiments?xsiType=xnat:mrSessionData&project={}&label={}&columns=xnat:imageSessionData/acquisition_site'.format(
                p, session['label'])
            json = intradb.getJson(uri)

            try:
                acq_site = json[0]['xnat:imagesessiondata/acquisition_site']
            except Exception as e:
                print "Couldn't get acquisition site for {}".format(session)
                print e.message
                acq_site = "NULL Site"

            # Get scan data including UUID
            uri = "{}/scans?format=json&columns=ID,xnat:mrScanData/type,xnat:mrScanData/fileNameUUID".format(session['URI'])
            scans = intradb.getJson(uri)

            for s in scans:
                # Exclude sbref scans since it's a duplicate of the counterpart
                if 'sbref' in s['xnat:mrscandata/type'].lower():
                    continue

                key = 'xnat:mrscandata/filenameuuid'
                # Check that this scan has a UUID and is the proper num chars
                if key not in s or len(s[key]) != 36:
                    continue

                uuid_map = dict()
                uuid_map['acquisition_site'] = acq_site
                uuid_map['project'] = p
                uuid_map['session'] = session['label']
                uuid_map['insert_date'] = session['insert_date']
                uuid_map['scan_date'] = session['date']
                uuid_map['scan_id'] = s['ID']
                # uuid_map['scan_type'] = a
                uuid_map['uuid'] = s[key]

                uuids.append(uuid_map)

    return uuids


def intradbBackupReport(aspera_uuids, xnat_uuids):
    uuid_list = []
    fname = '/owncloud/relay-common/stats/intradb-backup-uuids'

    backed_up_uuids = set()
    for uuid in aspera_uuids:
        backed_up_uuids.add(uuid['uuid'])

    # for uuid in xnat_uuids:
    #     on_xnat.add(uuid['uuid'])

    csv_header = "Status,AcqSite,Project,Session,ScanId,ScanDate,InsertDate,UUID\n"
    with open(fname+'.csv', 'w') as f:
        f.write(csv_header)

    for uuid in xnat_uuids:
        message = "MISSING RAW"
        if uuid['uuid'] in backed_up_uuids:
            message = "HAS RAW"

        uuid['message'] = message
        uuid_list.append(uuid)

        with open(fname+".csv", 'a') as f:
            line = "{},{},{},{},{},{},{},{}\n".format(
                uuid['message'],
                uuid['acquisition_site'],
                uuid['project'],
                uuid['session'],
                uuid['scan_id'],
                uuid['scan_date'],
                uuid['insert_date'],
                uuid['uuid']
            )
            f.write(line)

    with open(fname+'.json', 'w') as f:
        json.dump(uuid_list, f)

    # sync_uuids = on_aspera & on_xnat


if __name__ == "__main__":
    main()
