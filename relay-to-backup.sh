#!/bin/bash

# Move RAW data to backup on Aspera node every night

# More setup information can be found on the wiki below
# https://wiki.humanconnectome.org/display/storage/NRG+Image+Relay+Setup

# bbcp install
# https://www.olcf.ornl.gov/kb_articles/transferring-data-with-bbcp/

source /raw/scripts/sync-status.sh
source /opt/ozmt/zfs-tools-init.sh
# include and parse the yaml config file
source /raw/scripts/parse-config.sh
eval $(parse_yaml /raw/scripts/config.yaml)

logfile=$sync_logfile
report_name=$sync_report_name
running_flag=/tmp/raw-sync-running

SYNC_HOST=$sync_host
# Destination directories must already exist
RAW_SOURCE=$relay_source
RAW_DESTINATION=$relay_destination/${HOSTNAME}

# Comment if you do not want to delete from relay after Aspera transfer
# remove_after_transfer="--remove-after-transfer"

[ -f /etc/sysconfig/relay ] && source /etc/sysconfig/relay

main () {
  notice "Start syncing Relay to Backup started"
  ping_destination

  if [ -f $running_flag ]; then
    notice "Relay to Backup sync initiated while another was running"
    exit 1
  fi
  touch $running_flag

  sync_images $RAW_SOURCE $RAW_DESTINATION "RAW"

  rm -f $running_flag
  notice "Relay to Backup sync complete"

  exit 0
}

collect_ascp_tcpdump () {
  local to=
  if [ "$1" == '' ]; then
    to=15
  else
    to="$1"
  fi
  timeout $to /usr/sbin/tcpdump  -i enp0s20f0 -nn -v host 65.254.100.36 > /tmp/tcpdump.out 2>&1
}

ping_destination () {
  ~/.aspera/connect/bin/ascp -v -P 33001 -l 10G -i ~/.ssh/id_rsa /bin/false \
    $SYNC_HOST:/tmp/${HOSTNAME} 1>/tmp/connection.test.txt 2>&1
  if [ "$?" -ne "0" ]; then
    # Get more info about connectivity issue
    traceroute -n asp-connect1.wustl.edu >> /tmp/connection.test.txt
    ping -c 2 asp-connect1.wustl.edu >> /tmp/connection.test.txt
    collect_ascp_tcpdump 10s &
    sleep 0.5
    nmap --host-timeout 5s -sTU -p 33001 asp-connect1.wustl.edu >> /tmp/connection.test.txt
    sleep 15s
    cat /tmp/tcpdump.out >> /tmp/connection.test.txt
    error "Destination $SYNC_HOST unreachable" /tmp/connection.test.txt
    set_monitor_status "RAW" "failed" "Destination $SYNC_HOST unreachable" 0 "${HOSTNAME}"
    exit 1
  else
    date > /tmp/ping_asp-connect.txt
  fi
  rm /tmp/connection.test.txt
}

sync_images () {
  source_dir=$1
  dest_dir=$2
  image_type=$3 # (RAW, DICOM, or TWIX)
  error_file=/tmp/ascp.error.txt
  message_file=/tmp/ascp.stdout.txt

  notice "Getting files with last access time 30+ minutes ago."
  find $RAW_SOURCE -amin +30 -name "MR_*.cplxraw" > /tmp/raw-sync-list.txt

  if [ ! -s /tmp/raw-sync-list.txt ]; then
    notice "No files to sync from Image Relay ${HOSTNAME}."
    rm -f $running_flag
    exit 0
  fi

  start=$(date +%s)

  notice "$image_type data sync started on $SYNC_HOST"
  set_monitor_status $image_type "in progress" "" 0 "${HOSTNAME}"

  #rsync -rzcv --log-file=$LOG $source_dir $SYNC_HOST:$dest_dir

  #bbcp -rae -l $LOG -P 2 -w 2M -s 10 $source_dir $SYNC_HOST:$dest_dir

  sync_command="~/.aspera/connect/bin/ascp -i ~/.ssh/id_rsa -P 33001 -l 10G \
    -p --file-list=/tmp/raw-sync-list.txt --overwrite=diff -k 2 --mode=send \
    -L /raw/data/logs/ $remove_after_transfer --host=$SYNC_HOST $dest_dir \
    2>$error_file 1>$message_file"

  notice "$sync_command"
  eval $sync_command
  sync_exit=$?

  # Clean up output files, causing problems when parsing JSON
  #sed -e 's/^M//g' $error_file > $error_file
  #sed -e 's/^M//g' $message_file > $message_file

  if [ "$sync_exit" -eq "0" ]; then
    status="success"
    message=$( tail -n 1 $message_file ) # last line of stdout
    notice "$image_type data copied successfully to $SYNC_HOST"
  else
    status="failed"
    message=$( tail -n 1 $message_file 2>/dev/null )
    error "$image_type data sync failed (return status $sync_exit)" $error_file
  fi

  rm -f /tmp/raw-file-list.txt

  end=$(date +%s)
  seconds=$(echo "$end-$start" | bc)

  notice "Relay to Backup sync complete in $seconds seconds."

  set_monitor_status $image_type "${status}" "${message}" $seconds "${HOSTNAME}"
}

main
