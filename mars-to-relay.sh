#!/bin/bash

# Syncs from MARS computer to staging area on relay and deletes from MARS

# More setup information can be found on the wiki below
# https://wiki.humanconnectome.org/display/storage/NRG+Image+Relay+Setup

# logging and email functionality
source /opt/ozmt/zfs-tools-init.sh
# optional sync status to web app functionality
source /raw/scripts/sync-status.sh
# include and parse the yaml config file
source /raw/scripts/parse-config.sh
eval $(parse_yaml /raw/scripts/config.yaml)

# Exit if there's not MARS config section
if [ -z "$mars_ip" ]; then
  echo "No mars_ip in yaml"
  exit 0
fi

# parse_yaml is pulling multiples for lists, gets unique elements
#for ip in $(printf "%s\n" "${mars_ip[@]}" | sort -u); do
#  echo $ip
#done

umask 0027

logfile=$sync_logfile
report_name=$sync_report_name
running_flag=/tmp/mars-sync-running

# Rsync options, uncomment and set value in config.yaml to enable them
# bwlimit=$mars_bwlimit
remove_source_files=$mars_cleanup

[ -f /etc/sysconfig/relay ] && source /etc/sysconfig/relay

main () {
  notice "Starting sync from MARS to Relay"

  if [ -f $running_flag ]; then
    notice "MARS to Relay sync initiated while another was running"
    exit 1
  fi
  # Create flag when sync is running so we only have one rsync proc at a time
  touch $running_flag

  # parse_yaml is pulling multiples for lists, gets unique elements
  for ip in $(printf "%s\n" "${mars_ip[@]}" | sort -u); do
    echo $ip
    sync_images $ip
  done  

  rm -f $running_flag
  rm -f /tmp/mars-sync-list.txt
  notice "MARS to Relay sync completed $(date)"
}

sync_images () {
  mars_ip=$1
  notice "Attempting sync from MARS IP -- $mars_ip"

  # See if we're keeping in sync with an XNAT, i.e.,
  # only pulling data we have dicoms for in the XNAT in config.yaml
  
  if [ -n "$xnat_url" ]; then
    notice "Getting files in XNAT archive on $xnat_url to sync from MARS"
    # Run together for combined exit status
    python /raw/scripts/find-xnat-raw.py $mars_ip > /tmp/xnat-mars-intersect.txt && \
    scp /tmp/xnat-mars-intersect.txt root@$mars_ip:/tmp/mars-sync-list.txt
  else
    # If not keeping in sync with an XNAT, use simple 'find' command
    notice "Not keeping in sync with XNAT. Get /rawdata +60 min from mars."
    ssh root@$mars_ip 'find /rawdata -mmin +60 -name "MR_*.cplxraw"' | \
      sed 's,/rawdata/,,g' > /tmp/mars-sync-list.txt
  fi
  
  exit_status="$?"
  
  if [ $exit_status -ne "0" ]; then
    error "Couldn't create list of RAW files to sync on MARS -- scp return status: $exit_status"
    rm -f $running_flag
    return 1
  fi
  
  if [ ! -s /tmp/xnat-mars-intersect.txt ]; then
    notice "No files to sync from MARS."
    rm -f $running_flag
    return 0
  fi
  
  # Sync the list and remove files if successful
  start=$(date +%s)
  
  error_file=/tmp/mars.error.txt
  message_file=/tmp/mars.stdout.txt
  
  set_monitor_status "MARS" "in progress" "" 0 "${HOSTNAME}"
  
  # Launch command on MARS to sync on relay rsync server, should save some CPU cycles
  rsync_cmd="ssh root@$mars_ip 'rsync -tivh $bwlimit $remove_source_files \
    --files-from=/tmp/mars-sync-list.txt /rawdata/ rsync://$relay_ip:/staging/' \
    2>$error_file 1>$message_file"
  
  notice "$rsync_cmd"
  eval $rsync_cmd
  
  exit_status="$?"
  
  # Check the rsync return status and notify appropriately
  if [ $exit_status -eq "0" ]; then
    # second to last line of stdout
    message=$( tail -n 2 $message_file | head -n 1 )
    notice "MARS data copied successfully to ${HOSTNAME} $seconds seconds"
    status="success"
  else
    message=$( tail $error_file )
    error "MARS data sync failed (return status $exit_status)" $error_file
    status="failed"
  fi
  
  end=$(date +%s)
  seconds=$(echo "$end-$start" | bc)
  notice "MARS to Relay sync complete in $seconds seconds"
  
  notice "setting MARS monitor status"
  set_monitor_status "MARS" "$status" "$message" $seconds "${HOSTNAME}"
}

main
