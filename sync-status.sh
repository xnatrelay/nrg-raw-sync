#!/bin/bash

source /root/.bashrc
source ~/.bashrc

# Pushes optional sync status info for consupmtion by a web application

source /opt/ozmt/zfs-tools-init.sh
# include and parse the yaml config file
source /raw/scripts/parse-config.sh
eval $(parse_yaml /raw/scripts/config.yaml)

# Exit if there's no WEB config section
if [ -z "$web_host" ]; then
  exit 0
fi

logfile=$sync_logfile
report_name=$sync_report_name
WEB_HOST=$web_host
WEB_LOG=$web_log

[ -f /etc/sysconfig/relay ] && source /etc/sysconfig/relay

ping_webhost () {
  debug "ping -q -c 1 $WEB_HOST 2>/tmp/ping.test.txt"
  ping -q -c 1 $WEB_HOST 2>/tmp/ping.test.txt 1>/dev/null
  if [ "$?" -ne "0" ]; then
    # log_message "ERROR" "web host $WEB_HOST down"
    error "web host $WEB_HOST down" /tmp/ping.test.txt
    # exit 1, don't really want to exit just because web host is down
    rm -f /tmp/ping.test.txt
    WEB_HOST="" # unset host so we quit trying to connect
  fi
}

set_monitor_status () {
  image_type=$1 # (RAW, DICOM, or TWIX)
  sync_status=$2
  sync_message=$3
  seconds=$4
  host=$5

  json_body="{\"sync-host\": \"$host\", \"image-type\": \"$image_type\", \"status\": \"$sync_status\", \"message\": \"$sync_message\", \"last-updated\": \"$( date )\", \"elapsed\": \"$seconds\"}"

  if [ "$WEB_HOST" ]; then
    #ping_webhost
    # if [ "$?" -ne "0" ]; then
    #   notice "failed to ping webhost"
    #   return
    # fi

    # notice "Setting monitor status on $WEB_HOST"
    #echo $json_body | ssh -p 922 ${host}_sync@$WEB_HOST "cat > $fname && chmod 644 $fname"
    # Upload json file over https request instead of ssh
    json_file=$host-$image_type-status.json
    echo $json_body > /tmp/$json_file

    notice "Trying to send status file ($WEB_LOG/$json_file) to $WEB_HOST"
    curl -X POST -F "file=@/tmp/$json_file" https://$WEB_HOST/api/relay/transfers?filename=$WEB_LOG/$json_file

    exit_status=$?

    if [ "$exit_status" -ne "0" ]; then
      error "FAILED setting monitor status. Command returned $exit_status"
    else
      notice "Successfully set monitor status for $host on $WEB_HOST"
    fi
  fi
}
