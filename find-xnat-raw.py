#!/usr/bin/env python

'''
Generates a list of raw scans to sync from MARS computer based on what is in
the destination XNAT archive projects.
This must run on the remote image relay that is on the same network as MaRS
'''

import os
import sys
import yaml
import subprocess
import requests
from datetime import datetime
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from hcpxnat.interface import HcpInterface

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

with open("/raw/scripts/config.yaml") as f:
    cfg = yaml.load(f)

# See https://github.com/revmic/hcpxnat for details on this config file
cfg_file = os.path.join(os.path.expanduser('~'), ".hcpxnat_intradb.cfg")
intradb = HcpInterface(config=cfg_file)
intradb.url = cfg['xnat']['url']
xnat_projects = cfg['xnat']['projects']
# mars_ip = cfg['mars']['ip']
# Make sure we use newest log since ozmt appends year-month
log_file = cfg['sync']['logfile'] + "." + datetime.now().strftime("%Y-%m")

# Now getting mars_ip as a paramter since it can be a list of multiple IPs
try:
    mars_ip = sys.argv[1]
except IndexError as e:
    with open(log_file, 'a') as l:
        l.write(e.message + '\n')
        l.write("find-xnat-raw.py requires a MARS IP as a parameter\n")
    sys.exit(1)


def main():
    mars_uuids = getMarsUuids()
    xnat_uuids = getXnatUuids()
    # Sync any raw data on MARS that is also in destination XNAT archive
    printSyncList(mars_uuids, xnat_uuids)
    sys.exit(0)


def getMarsUuids():
    """
    Parses and returns the UUIDs from the MARs file listing
    """
    command = "ssh root@{ip} 'ls -1 /rawdata/'".format(ip=mars_ip)
    result = subprocess.check_output(command, shell=True)
    files = result.split('\n')[:-1]  # Exclude empty string from list
    uuids = []

    for f in files:
        uuid_map = dict()
        uuid_map['filename'] = f
        try:
            uuid_str = f.split('_')[3]
        except Exception as e:
            with open(log_file, 'a') as l:
                message = "{} isn't a cplxraw file or improper file name format\n"
                l.write(message.format(f))
                l.write(e.message + '\n')
            continue

        if len(uuid_str) == 36:
            uuid_map['uuid'] = uuid_str
            uuids.append(uuid_map)
        else:
            e = "ERROR adding " + uuid_str + ". Likely not a UUID"
            with open(log_file, 'a') as f:
                f.write(e)

    # print "\n-- MARS UUIDS --"
    # print len(uuids)
    # print uuids
    return uuids


def getXnatUuids():
    """
    Get the set of UUIDs for each scan in each session in each project
    """
    uuids = []

    for p in xnat_projects:
        # print "\n=== Project {} ===\n".format(p)
        sessions = intradb.getSessions(project=p)
        # print sessions

        for session in sessions:
            # print session['URI']
            # print "\n=== Session {} ===\n".format(session['label'])
            uri = "{}/scans?format=json&columns=ID,xnat:mrScanData/fileNameUUID".format(session['URI'])
            # print uri
            scans = intradb.getJson(uri)

            for s in scans:
                key = 'xnat:mrscandata/filenameuuid'
                # Check that this scan has a UUID and is the proper num chars
                if key not in s or len(s[key]) != 36:
                    continue

                uuid_map = dict()
                uuid_map['project'] = p
                uuid_map['session'] = session['label']
                uuid_map['scan_id'] = s['ID']
                # uuid_map['scan_type'] = a
                uuid_map['uuid'] = s[key]

                uuids.append(uuid_map)

    # print "\n-- XNAT UUIDS --"
    # print len(uuids)
    # print uuids
    return uuids


def printSyncList(mars_uuids, xnat_uuids):
    """
    Get the file names of the intersecting UUIDs and print.
    """
    # Find the intersection of MARS and XNAT UUIDs
    on_mars = set()
    for uuid in mars_uuids:
        on_mars.add(uuid['uuid'])

    on_xnat = set()
    for uuid in xnat_uuids:
        on_xnat.add(uuid['uuid'])

    sync_uuids = on_mars & on_xnat

    # Get the filenames for the intersecting UUIDs and print to stdout
    sync_files = []
    for mars in mars_uuids:
        if mars['uuid'] in sync_uuids:
            sync_files.append(os.path.basename(mars['filename']))

    for f in sync_files:
        print f


if __name__ == "__main__":
    main()
