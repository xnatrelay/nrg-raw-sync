# NRG Raw k-space Raw sync #

These scripts facilitate backing up .cplxraw files to Washington University from various imaging sites. A status overview of currently configured relays can be found at https://hcp-ops.humanconnectome.org/relays

### Nodes of Interest ###

* MaRS reconstruction computer
* NRG image relay
* Aspera backup server

### Main Scripts ###

* Create a copy of __sample-config.yaml__ as __config.yaml__ which contains all the various config parameters.
* __find-xnat-raw.py__ -- Queries an XNAT and gets a list of scan UUIDs for a list of projects in config.yaml, gets a list of files on the MaRS which have UUIDs in filenames, and gets the intersection of these UUIDs. These are the files we want to backup.
* __mars-to-relay.sh__ -- Calls find-xnat-raw.py and rsyncs the resulting list of files to the Image Relay staging area (also defined in config.yaml)
* __relay-to-backup.sh__ -- Finds files on the Relay in the staging area that haven't been accessed recently and copies these files to the Backup Server at WashU using ascp over the Aspera Transport Protocol (http://asperasoft.com/technology/transport/fasp/).
* __cleanup-relay.py__ -- Removes DICOM sessions from the relay XNAT